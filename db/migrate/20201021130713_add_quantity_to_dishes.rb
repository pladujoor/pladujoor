class AddQuantityToDishes < ActiveRecord::Migration[6.0]
  def change
    add_column :dishes, :quantity, :integer
  end
end
