class AddRestaurantToCategories < ActiveRecord::Migration[6.0]
  def change
    add_column :categories, :restaurant_id, :bigint
    add_index :categories, :restaurant_id
  end
end
