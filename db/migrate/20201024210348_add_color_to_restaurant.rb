class AddColorToRestaurant < ActiveRecord::Migration[6.0]
  def change
    add_column :restaurants, :color1, :string
    add_column :restaurants, :color2, :string
    add_column :restaurants, :color3, :string
    add_column :restaurants, :address, :string
    add_column :restaurants, :email, :string
    add_column :restaurants, :telephone, :string
    add_column :restaurants, :ownerfirstname, :string
    add_column :restaurants, :ownerlastname, :string
  end
end
