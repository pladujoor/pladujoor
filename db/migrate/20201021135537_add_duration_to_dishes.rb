class AddDurationToDishes < ActiveRecord::Migration[6.0]
  def change
    add_column :dishes, :duration, :integer
  end
end
