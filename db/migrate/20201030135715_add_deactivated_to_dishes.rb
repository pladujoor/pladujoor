class AddDeactivatedToDishes < ActiveRecord::Migration[6.0]
  def change
    add_column :dishes, :deactivated, :boolean
  end
end
