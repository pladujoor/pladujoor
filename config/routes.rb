Rails.application.routes.draw do
  get 'qr_codes/new'
  get 'qr_codes/create'
  # devise_for :users
  # devise_for :users, controllers: {
  #   sessions: 'users/sessions'
  # }
  devise_for :users do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  get 'basic-qr-code-reader', to: 'basic_qr_codes#index'
  root to: 'pages#home' do
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  resources :restaurants do
    resources :orders
    resources :tables do
      resources :orders do
        resources :dishes
        resources :people
      end
    end
    resources :menus do
      resources :categories do
        resources :dishes
      end
      resources :dishes
    end
    resources :dishes
  end

  # resources :tables do
  #   resources :orders, only: [:index, :new, :create, :show, :edit, :update] do
  #     member do
  #       get :confirmation
  #     end
  #   end
  # end

  resources :restaurants do
    resources :tables do
      resources :orders do
        member do
          get :confirmation
        end
      end
    end
  end

  resources :order_dishes, only: [:edit]

  get :ready_order_dish, to: 'order_dishes#ready'

  get :pay_order_dish, to: 'order_dishes#pay'

  get :paid_order_dish, to: 'order_dishes#paid'

  get :command_order_dish, to: 'order_dishes#command'

  get :cook_order_dish, to: 'order_dishes#cook'

  # get :add_order_dish, to: 'orders#addone', as: 'command'
  # get 'orders/:order_id/dishes/:id/order_dishes/new', to: 'orders#addone', as: 'command'
  get 'restaurants/:restaurant_id/tables/:table_id/orders/:order_id/order_dishes/new', to: 'orders#addone', as: 'command'
  get 'restaurants/:restaurant_id/tables/:table_id/orders/:order_id/confirm', to: 'orders#confirm', as: 'confirm'
  get 'restaurants/:restaurant_id/tables/:table_id/orders/:order_id/dishes/:id/order_dishes/new', to: 'dishes#seeone', as: 'see'
  #deete people
  get 'restaurants/:restaurant_id/tables/:table_id/orders/:order_id/people/:id/destroy', to: 'people#destroy', as: 'destroyperson', data: { confirm: "Are you sure?" }
  #delete table
  get 'restaurants/:restaurant_id/tables/:id/destroy', to: 'tables#destroy', as: 'destroytable', data: { confirm: "Are you sure?" }
  #delete catagory
  get 'restaurants/:restaurant_id/menus/:menu_id/categories/:id/destroy', to: 'categories#destroy', as: 'destroycategory', :data => { :confirm => "Are you sure?" }
  # delete dish
  get 'restaurants/:restaurant_id/dishes/:id/destroy', to: 'dishes#destroy', as: 'destroydish', :data => { :confirm => "Are you sure?" }
  #route for pdf
  get 'restaurants/:restaurant_id/tables/:table_id/qr.pdf', to: 'tables#qr', as: 'qr'
  #activate menu
  get 'restaurants/:restaurant_id/menus/:menu_id/activate', to: 'menus#activate', as: 'activemenu', :data => { :confirm => "Are you sure?" }

  resources :orders do
    resources :dishes, only: :show
  end

  resources :qr_codes, only: [:new, :create]

  get 'qr_codes', to: "qr_codes#new"

  resources :tables

  get '/design', to: 'pages#design'



end
