require 'rqrcode'
require 'rqrcode_core'

prawn_document do |pdf|
  pdf.text "Hello #{@restaurant.name}"

@tables.each do |table|

#rqrcode

qrcode = RQRCode::QRCode.new("http://pladujoor.xyz/tables/#{table.id}/orders/new")

# NOTE: showing with default options specified explicitly
png = qrcode.as_png(
  bit_depth: 1,
  border_modules: 4,
  color_mode: ChunkyPNG::COLOR_GRAYSCALE,
  color: 'black',
  file: nil,
  fill: 'white',
  module_px_size: 6,
  resize_exactly_to: false,
  resize_gte_to: false,
  size: 120
)

IO.binwrite("/home/stephanefullstack/pladujoor/app/views/tables/table_#{table.id}.png", png.to_s)

pdf.text "Table #{table.id}"
pdf.image "/home/stephanefullstack/pladujoor/app/views/tables/table_#{table.id}.png", width: 200

end
#saut de page
# pdf.start_new_page size: "A4", page_layout: :portrait

end
