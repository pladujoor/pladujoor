class OrderDishPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def show?
    return true
  end

  def edit?
    return true
  end

  def cook?
    return true
  end

  def ready?
    return true
  end

  def pay?
    return true
  end

  def paid?
    return true
  end


  def command?
    return true
  end
end
