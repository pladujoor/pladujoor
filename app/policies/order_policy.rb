class OrderPolicy < ApplicationPolicy

  def new?
    return true
  end

  def create?
    return true
  end

  def show?
    return true
  end

  def confirmation?
    return true
  end

  def edit?
    return true
  end

  def update?
    return true
  end

  def addone?
    return true
  end

  def confirm?
    return true
  end

  def index?
    return true
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
