class Table < ApplicationRecord
  has_many :orders
  belongs_to :restaurant
  has_many :order_dishes, through: :orders
  # has_attachment  :qrcode, accept: [:jpg, :png, :gif]
  # has_attachments :photos, maximum: 10
  # validates :avatar, presence: true
  has_one_attached :qrcode
end
