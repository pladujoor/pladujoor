class Order < ApplicationRecord
  has_many :order_dishes
  has_many :dishes, through: :order_dishes
  has_many :persons
  belongs_to :table

end
