class Restaurant < ApplicationRecord
  has_one_attached :photo
  has_many :tables
  has_many :orders, through: :tables
  has_many :order_dishes, through: :orders
  has_many :menus
  has_many :dishes
  belongs_to :user
end
