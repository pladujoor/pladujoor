class OrderDishesController < ApplicationController
  def index
    @orderdishes = OrderDish.all
  end

  def show
    @orderdish = OrderDish.find(params[:id])
    authorize @orderdish
  end

  def edit
    @orderdish = OrderDish.find(params[:id])
    authorize @orderdish
  end

  def cook
    @orderdish = OrderDish.find(params[:format])
    @orderdish.status = "En préparation"
    @orderdish.save
    redirect_to orders_path
    authorize @orderdish
  end

  def ready
    @orderdish = OrderDish.find(params[:format])
    @orderdish.status = "Prêt"
    @orderdish.save
    redirect_to orders_path
    authorize @orderdish
  end

  def pay
    @orderdish = OrderDish.find(params[:format])
    @orderdish.status = "A payer"
    @orderdish.save
    redirect_to orders_path
    authorize @orderdish
  end

  def paid
    @orderdish = OrderDish.find(params[:format])
    @orderdish.status = "Payé"
    @orderdish.save
    redirect_to orders_path
    authorize @orderdish
  end

  def command
    @orderdish = OrderDish.find(params[:format])
    @orderdish.status = "Commandé"
    @orderdish.save
    redirect_to orders_path
    authorize @orderdish
  end

end
