class CategoriesController < ApplicationController
  def index
    @categories = policy_scope(Category)
  end
  def show
    @category = Category.find(params[:id])
    @menu = Menu.find(params[:menu_id])
    authorize @category
  end

  def new
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menu = Menu.find(params[:menu_id])
    @category = Category.new
    authorize @category
  end

  def create
    @category = Category.new(category_params)
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menu = Menu.find(params[:menu_id])
    @category.menu = @menu
    @category.restaurant = @restaurant
    authorize @category
    if @category.save!
      redirect_to new_restaurant_menu_category_path(@menu.restaurant, @menu)
    else
      render 'new'
    end
  end

  def destroy
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menu = Menu.find(params[:menu_id])
    @category = Category.find(params[:id])
    if @category.dishes.first == nil
      @category.destroy
    else

    end
    authorize @category
    redirect_to new_restaurant_menu_category_path(@restaurant, @menu)
  end

  def edit
    @category = Category.find(params[:id])
    authorize @category
  end

  def update
    @category = Category.find(params[:id])
    authorize @category
    @category.update(category_params)
    redirect_to category_path(@category)
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end

  def set_menu
    @menu = Menu.find(params[:menu_id])
  end
end
