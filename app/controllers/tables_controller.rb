class TablesController < ApplicationController
  def index
    @tables = policy_scope(Table)
    @table = Table.new
    @restaurant = Restaurant.find(params[:restaurant_id])
  end

  def show
    if params[:id] == "qr"
      @restaurant =Restaurant.find(params[:restaurant_id])
      @tables = Table.all
      authorize @tables
    else
      @restaurant =Restaurant.find(params[:restaurant_id])
      @table = Table.find(params[:id])
      @orders = @table.orders
      authorize @table
    end
  end

  def new
    @restaurant = Restaurant.find(params[:restaurant_id])
    @table = Table.new
    authorize @table
  end

  def qr
    # require "prawn"
    @tables = Table.all
    @table = Table.find(params[:table_id])
    @restaurant = @table.restaurant
    authorize @table
  end

  def destroy
    @table = Table.find(params[:id])
    @restaurant = @table.restaurant
    @table.destroy
    authorize @table
    redirect_to restaurant_tables_path(@restaurant)
  end

  def create
    @url = request.original_url
    @restaurant = Restaurant.find(params[:restaurant_id])
    x = 1
    while @restaurant.tables.find_by(number: x) != nil do
        x += 1
        end
      @table = Table.new(number: x)
      # @table = Table.new(table_params)
      @table.restaurant = Restaurant.find(params[:restaurant_id])
      authorize @table

      qrcode = RQRCode::QRCode.new("http://pladujoor.xyz/tables/table_#{@table.id}/orders/new")

      # NOTE: showing with default options specified explicitly
      png = qrcode.as_png(
        bit_depth: 1,
        border_modules: 4,
        color_mode: ChunkyPNG::COLOR_GRAYSCALE,
        color: 'black',
        file: nil,
        fill: 'white',
        module_px_size: 6,
        resize_exactly_to: false,
        resize_gte_to: false,
        size: 120
      )

      IO.binwrite("app/assets/images/table_#{@table.number}.png", png.to_s)
      # @qrcode = "tables/table_#{@table.number}.png"

      if @table.save!
        redirect_to restaurant_tables_path(@restaurant)
      else
        render 'new'
      end
    end


  end

  def edit
    @table = Table.find(params[:id])
    authorize @table
  end

  def update
      @table = Table.find(params[:id])
    @table.save
    redirect_to table_path(@table)
  end

  private

  def table_params
    params.require(:table).permit(:number, :qrcode)
  end
