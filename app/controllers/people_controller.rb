class PeopleController < ApplicationController
  skip_before_action :authenticate_user!, only: [:new, :create, :show, :confirmation, :edit, :update ]
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    @people = policy_scope(Person)
  end

  # GET /people/1
  # GET /people/1.json
  def show
    @person = Person.find(params[:id])
    authorize @person
  end

  # GET /people/new
  def new
    @person = Person.new
    @person.order = Order.find(params[:order_id])
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)
    @person.order = Order.find(params[:order_id])
    authorize @person
    @person.save
    # respond_to do |format|
    #   if @person.save
    #     format.html { redirect_to @person, notice: 'Person was successfully created.' }
    #     format.json { render :show, status: :created, location: @person }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @person.errors, status: :unprocessable_entity }
    #   end
    # end
    redirect_to restaurant_table_order_path(@person.order.table.restaurant, @person.order.table, @person.order)
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    # respond_to do |format|
    #   if @person.update(person_params)
    #     format.html { redirect_to @person, notice: 'Person was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @person }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @person.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    # respond_to do |format|
    #   format.html { redirect_to people_url, notice: 'Person was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
    @person = Person.find(params[:id])
    authorize @person
    @person.destroy
    redirect_to restaurant_table_order_people_path(@person.order.table.restaurant, @person.order.table, @person.order)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def person_params
      # params.fetch(:person, {})
      params.require(:person).permit(:firstname, :lastname, :phone)
    end
end
