class DishesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:show, :seeone]
  def index
    @restaurant = Restaurant.find(params[:restaurant_id])
    @dishes = policy_scope(Dish)
  end

  def show
    if params[:order_id] == nil
        @dish = Dish.find(params[:id])
        authorize @dish
      else
        @order = Order.find(params[:order_id])
        @table = @order.table
        @dish = Dish.find(params[:id])
        @restaurant = Restaurant.find(params[:restaurant_id])
        @dishe_ids_with_quantity = @order.order_dishes.group(:dish_id).count
    # @order_dish = OrderDish.create(dish_id: params[:id], order: Order.find(params[:order_id]), status: "Commandé")
        authorize @dish
    end
  end

  def seeone
    @restaurant = Restaurant.find(params[:restaurant_id])
    @table = Table.find(params[:table_id])
    @order = Order.find(params[:order_id])
    @dish = Dish.find(params[:id])
    OrderDish.create(dish_id: @dish.id, order: @order, status: "Commandé")
    authorize @dish
    redirect_to edit_restaurant_table_order_path(@order.table.restaurant, @order.table, @order, anchor: "dish-#{@dish.id}")
  end

  def new
    @dish = Dish.new
    @restaurant = Restaurant.find(params[:restaurant_id])
    if Menu.find(params[:menu_id]) != nil
      @menu = Menu.find(params[:menu_id])
    end
    if Category.find(params[:category_id])!= nil
      @category = Category.find(params[:category_id])
      @dish.category = @category
    end
    authorize @dish
  end

  def create
    @restaurant = Restaurant.find(params[:restaurant_id])
    if @menu != nil
      @menu = Menu.find(params[:menu_id])
    end
    @dish = Dish.new(dish_params)
    @category = Category.find(params[:dish][:category_id])
    @dish.category_id = @category.id
    @dish.restaurant = @restaurant
    authorize @dish
    if @dish.save!
      redirect_to restaurant_menu_path(@restaurant, @menu)
    else
      render "new"
    end
  end

  def edit
    @restaurant = Restaurant.find(params[:restaurant_id])
    @dish = Dish.find(params[:id])
    authorize @dish
  end

  def update
    @dish = Dish.find(params[:id])
    authorize @dish
    @dish.update(dish_params)
    redirect_to restaurant_dish_path(@dish.restaurant, @dish)
  end

  def destroy
    @dish = Dish.find(params[:id])
    @restaurant = Restaurant.find(params[:restaurant_id])
    authorize @dish
    @dish.destroy
    redirect_to restaurant_dishes_path(@restaurant)
  end

  private

  def dish_params
    params.require(:dish).permit(:name, :ingredient, :quantity, :duration, :price, :photo, :category, :deactivated)
  end
end
