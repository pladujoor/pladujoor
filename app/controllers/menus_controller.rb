class MenusController < ApplicationController
  def index
    @manus = policy_scope(Menu)
    @menus = @manus.where(restaurant_id: params[:restaurant_id])
    @restaurant = Restaurant.find(params[:restaurant_id])
  end

  def show
    @menu = Menu.find(params[:id])
    @dishes = Dish.all
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menu = Menu.find(params[:id])
    authorize @menu
  end

  def new
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menu = Menu.new
    authorize @menu
  end

  def create
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menu = Menu.new(menu_params)
    @menu.restaurant = @restaurant
    if @menu.save!
      redirect_to restaurant_menus_path
      authorize @menu
    else
      render 'new'
    end
  end

  def destroy
    @menu = Menu.find(params[:id])
    authorize @menu
    @menu.destroy
    redirect_to restaurant_menus_path(@restaurant)
  end

  def edit
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menu = Menu.find(params[:id])
    @order = Order.find(params[:id])
    authorize @order
  end

  def update
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menu = Menu.find(params[:id])
    authorize @menu
    @menu.update(menu_params)
    redirect_to restaurant_menus_path(@restaurant)
  end

  def activate
    @restaurant = Restaurant.find(params[:restaurant_id])
    @menus = Menu.where(restaurant_id: @restaurant.id)
    @menuid = params[:menu_id]
    @menus.each do |menu|
      if menu.id.to_s == @menuid
        @menu = menu
        @menu.active = true
        @menu.name = menu.name
        @menu.save
        authorize @menu
      else
        @menu = menu
        @menu.active = false
        @menu.name = menu.name
        @menu.save
        authorize @menu
      end
    end
    # authorize @menus
    redirect_to restaurant_menus_path(@restaurant)
  end

  private

  def menu_params
    params.require(:menu).permit(:name, :active)
  end
end
